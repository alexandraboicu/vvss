import evaluator.controller.AppController;
import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.InputValidationFailedException;
import evaluator.exception.NotAbleToCreateTestException;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class TestsFo2 {
    static AppController appController;

    @BeforeClass
    public static void setUp() {
        appController = new AppController();

    }

    @Test
    public void F02_TC01(){

        try {
            appController.deleteIntrebari();
            appController.addIntrebare("Ce faci?",
                    "1)Bine",
                    "2)Rau",
                    "3)Foarte bine",
                    "3",
                    "Stare1");
            appController.addIntrebare("Ce faci2?",
                    "1)Bine",
                    "2)Rau",
                    "3)Foarte bine",
                    "3",
                    "Stare2");
            appController.addIntrebare("Ce faci3?",
                    "1)Bine",
                    "2)Rau",
                    "3)Foarte bine",
                    "3",
                    "Stare3");
            appController.addIntrebare("Ce faci4?",
                    "1)Bine",
                    "2)Rau",
                    "3)Foarte bine",
                    "3",
                    "Stare");

            appController.createNewTest();
        }catch(NotAbleToCreateTestException | DuplicateIntrebareException | InputValidationFailedException e){
            Assert.assertEquals(e.getMessage(),"Nu exista suficiente intrebari pentru crearea unui test!(5)");
        }
    }

    @Test
    public void F02_TC02(){

        try {
            appController.deleteIntrebari();
            appController.addIntrebare("Ce faci?",
                    "1)Bine",
                    "2)Rau",
                    "3)Foarte bine",
                    "3",
                    "Stare1");
            appController.addIntrebare("Ce faci2?",
                    "1)Bine",
                    "2)Rau",
                    "3)Foarte bine",
                    "3",
                    "Stare2");
            appController.addIntrebare("Ce faci3?",
                    "1)Bine",
                    "2)Rau",
                    "3)Foarte bine",
                    "3",
                    "Stare3");
            appController.addIntrebare("Ce faci4?",
                    "1)Bine",
                    "2)Rau",
                    "3)Foarte bine",
                    "3",
                    "Stare");
            appController.addIntrebare("Ce faci5?",
                    "1)Bine",
                    "2)Rau",
                    "3)Foarte bine",
                    "3",
                    "Stare");

            appController.createNewTest();
        }catch(NotAbleToCreateTestException | DuplicateIntrebareException | InputValidationFailedException e){
            Assert.assertEquals(e.getMessage(),"Nu exista suficiente domenii pentru crearea unui test!(5)");
        }
    }

    @Test
    public void F02_TC03() throws NotAbleToCreateTestException, DuplicateIntrebareException, InputValidationFailedException {


            appController.deleteIntrebari();
            appController.addIntrebare("Ce faci1?",
                    "1)Bine",
                    "2)Rau",
                    "3)Foarte bine",
                    "3",
                    "Stare1");
            appController.addIntrebare("Ce faci2?",
                    "1)Bine",
                    "2)Rau",
                    "3)Foarte bine",
                    "3",
                    "Stare2");
            appController.addIntrebare("Ce faci3?",
                    "1)Bine",
                    "2)Rau",
                    "3)Foarte bine",
                    "3",
                    "Stare3");
            appController.addIntrebare("Ce faci5?",
                    "1)Bine",
                    "2)Rau",
                    "3)Foarte bine",
                    "3",
                    "Stare4");
            appController.addIntrebare("Ce faci6?",
                    "1)Bine",
                    "2)Rau",
                    "3)Foarte bine",
                    "3",
                    "Stare5");

            Assert.assertEquals(appController.createNewTest().getIntrebari().size(),5);

    }
}
