package evaluator.main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.InputValidationFailedException;
import evaluator.exception.NotAbleToCreateTestException;
import evaluator.model.Intrebare;
import evaluator.model.Statistica;

import evaluator.controller.AppController;
import evaluator.exception.NotAbleToCreateStatisticsException;

//functionalitati
//F01.	 adaugarea unei noi intrebari pentru un anumit domeniu (enunt intrebare, raspuns 1, raspuns 2, raspuns 3, raspunsul corect, domeniul) in setul de intrebari disponibile;
//F02.	 crearea unui nou test (testul va contine 5 intrebari alese aleator din cele disponibile, din domenii diferite);
//F03.	 afisarea unei statistici cu numarul de intrebari organizate pe domenii.

public class StartApp {

	private static final String file = "intrebari.txt";
	
	public static void main(String[] args) throws IOException {
		
		BufferedReader console = new BufferedReader(new InputStreamReader(System.in));
		
		AppController appController = new AppController();
		
		boolean activ = true;
		String optiune = null;
		
		while(activ){
			
			System.out.println("");
			System.out.println("1.Adauga intrebare");
			System.out.println("2.Creeaza test");
			System.out.println("3.Statistica");
			System.out.println("4.Exit");
			System.out.println("");
			
			optiune = console.readLine();
			
			switch(optiune){
			case "1" :
				try {
					System.out.println("Dati intrebare (trebuie sa inceapa cu majuscula si sa se termine cu '?'");
					String intrebare=console.readLine();
					System.out.println("Dati varianta 1 (trebuie sa inceapa cu 1))");

					String varianta1=console.readLine();
					System.out.println("Dati varianta 2 (trebuie sa inceapa cu 2))");

					String varianta2=console.readLine();
					System.out.println("Dati varianta 3 (trebuie sa inceapa cu 3))");

					String varianta3=console.readLine();
					System.out.println("Dati raspunsul corect (trebuie sa fie 1,2 sau 3)");

					String raspuns=console.readLine();
					System.out.println("Dati domeniul (Trebuia sa inceapa cu litera mare)");

					String domeniu=console.readLine();
					Intrebare intrebare1=new Intrebare(intrebare,varianta1,varianta2,varianta3,raspuns,domeniu);
					appController.addNewIntrebare(intrebare1);
					appController.addIntrebareToFile(intrebare1,file);
				}catch (InputValidationFailedException|DuplicateIntrebareException e){
					e.getMessage();
				}
				break;
			case "2" :
				try {
					appController.createNewTest();
				}catch(NotAbleToCreateTestException e){
					System.out.println(e.getMessage());
				}
				break;
			case "3" :
				appController.loadIntrebariFromFile(file);
				Statistica statistica;
				try {
					statistica = appController.getStatistica();
					System.out.println(statistica);
				} catch (NotAbleToCreateStatisticsException e) {
					System.out.println(e.getMessage());
				}
				
				break;
			case "4" :
				activ = false;
				break;
			default:
				break;
			}
		}
		
	}

}
