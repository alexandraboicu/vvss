VVSS, Proiect 5. Evaluator Examen

5.Evaluator Examen

Un profesor doreste dezvoltarea unui program pentrugestionarea subiectelor pentru un test de tip grila. Programul va avea urmatoarele functionalitati:

F01. adaugarea unei noi intrebari pentru un anumit domeniu (enunt intrebare, rapuns1, raspuns 2, raspuns 3, raspunsul corect, domeniul) in setul de intrebari disponibile. Intrebarile vor fi salvate intr-un fisier “intrebari.txt”

F02. creare unui nou test ( testul va contine 5 intrebari alese aleator din cele disponibile, din domenii dierite)

F03. afisarea unei statistici cu numarul de intrebari organizate pe domenii in consola.

La deschiderea aplicatiei in consola va aparea un meniu pentru utilizator cu operatiile pe care le doreste sa le faca : Adauga intrebare, Creeaza test, Afiseaza statistica, Exit.