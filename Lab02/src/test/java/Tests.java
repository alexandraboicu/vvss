import evaluator.controller.AppController;
import evaluator.exception.InputValidationFailedException;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
public class Tests {
    static AppController appController;

    @BeforeClass
    public static void setUp() {
        appController = new AppController();
    }

    @Test
    public void TC1_ECP() throws Exception {
        appController.addIntrebare("Ce faci?",
                "1)Bine",
                "2)Rau",
                "3)Foarte bine",
                "3",
                "Stare");
    }

    @Test
    public void TC3_ECP() throws Exception {
        try {
            appController.addIntrebare("Ce faci?",
                    "1)Bine",
                    "2)Rau",
                    "3)Foarte bine",
                    "abc",
                    "Stare");
        }
        catch(InputValidationFailedException e){
            Assert.assertEquals(e.getMessage(),"Varianta corecta nu este unul dintre caracterele {'1', '2', '3'}");
        }
    }

    @Test
    public void TC5_ECP() throws Exception {
        try {
            appController.addIntrebare("Ce faci?",
                    "1)Bine",
                    "2)Rau",
                    "3)Foarte bine",
                    "2",
                    "");
        }
        catch(InputValidationFailedException e){
            Assert.assertEquals(e.getMessage(),"Domeniul este vid!");
        }
    }

    @Test
    public void TC7_ECP() throws Exception {
        try {
            appController.addIntrebare("Ce faci?",
                    "1)Bine",
                    "2)Rau",
                    "3)Foarte bine",
                    "2",
                    "Aajsdbjsvfhgkagjhvflhkalkasjdlhafkgvdvfaklfhiagfafkashfafjgsvdjhfdsjf");
        }
        catch(InputValidationFailedException e){
            Assert.assertEquals(e.getMessage(),"Lungimea domeniului depaseste 30 de caractere!");
        }
    }

    @Test
    public void TC3_BVA() throws Exception {
        try {
            appController.addIntrebare("Ce faci?",
                    "1)Bine",
                    "2)Rau",
                    "3)Foarte bine",
                    "?",
                    "Stare");
        }
        catch(InputValidationFailedException e){
            Assert.assertEquals(e.getMessage(),"Varianta corecta nu este unul dintre caracterele {'1', '2', '3'}");
        }
    }

    @Test
    public void TC4_BVA() throws Exception {
        try {
            appController.addIntrebare("Ce faci2?",
                    "1)Bine",
                    "2)Rau",
                    "3)Foarte bine",
                    "3",
                    "Stare");
        }
        catch(InputValidationFailedException e){
            Assert.assertEquals(e.getMessage(),"Varianta corecta nu este unul dintre caracterele {'1', '2', '3'}");
        }
    }

    @Test
    public void TC8_BVA() throws Exception {
        try {
            appController.addIntrebare("Ce faci?",
                    "1)Bine",
                    "2)Rau",
                    "3)Foarte bine",
                    "2",
                    "a");
        }
        catch(InputValidationFailedException e){
            Assert.assertEquals(e.getMessage(),"Prima litera din domeniu nu e majuscula!");
        }
    }
}



