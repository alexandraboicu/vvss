import evaluator.controller.AppController;
import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.InputValidationFailedException;
import evaluator.exception.NotAbleToCreateStatisticsException;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class TestsF03 {
    static AppController appController;

    @BeforeClass
    public static void setUp() {
        appController = new AppController();

    }

    @Test
    public void F03_valid() {

        try {
            appController.deleteIntrebari();
            appController.addIntrebare("Ce faci?",
                    "1)Bine",
                    "2)Rau",
                    "3)Foarte bine",
                    "3",
                    "Stare1");
            appController.addIntrebare("Ce faci2?",
                    "1)Bine",
                    "2)Rau",
                    "3)Foarte bine",
                    "3",
                    "Stare2");
            appController.addIntrebare("Ce faci3?",
                    "1)Bine",
                    "2)Rau",
                    "3)Foarte bine",
                    "3",
                    "Stare3");
            appController.addIntrebare("Ce faci4?",
                    "1)Bine",
                    "2)Rau",
                    "3)Foarte bine",
                    "3",
                    "Stare");

            appController.getStatistica();
        } catch (DuplicateIntrebareException | InputValidationFailedException | NotAbleToCreateStatisticsException e) {
        }
    }

    @Test
    public void F03_invalid() {

        try {
            appController.deleteIntrebari();
            appController.getStatistica();

        } catch (NotAbleToCreateStatisticsException e) {
            Assert.assertEquals(e.getMessage(), "Repository-ul nu contine nicio intrebare!");
        }
    }

}
