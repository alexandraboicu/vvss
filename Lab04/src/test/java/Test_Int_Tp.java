import evaluator.controller.AppController;
import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.InputValidationFailedException;
import evaluator.exception.NotAbleToCreateStatisticsException;
import evaluator.exception.NotAbleToCreateTestException;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class Test_Int_Tp {
    static AppController appController;

    @BeforeClass
    public static void setUp() {
        appController = new AppController();

    }

    @Test
    public void testF01() throws DuplicateIntrebareException, InputValidationFailedException {
        appController.addIntrebare("Ce faci?",
                "1)Bine",
                "2)Rau",
                "3)Foarte bine",
                "3",
                "Stare");
        appController.deleteIntrebari();
    }

    @Test
    public void testF02() throws DuplicateIntrebareException, InputValidationFailedException, NotAbleToCreateTestException {
        appController.deleteIntrebari();
        appController.addIntrebare("Ce faci1?",
                "1)Bine",
                "2)Rau",
                "3)Foarte bine",
                "3",
                "Stare1");
        appController.addIntrebare("Ce faci2?",
                "1)Bine",
                "2)Rau",
                "3)Foarte bine",
                "3",
                "Stare2");
        appController.addIntrebare("Ce faci3?",
                "1)Bine",
                "2)Rau",
                "3)Foarte bine",
                "3",
                "Stare3");
        appController.addIntrebare("Ce faci5?",
                "1)Bine",
                "2)Rau",
                "3)Foarte bine",
                "3",
                "Stare4");
        appController.addIntrebare("Ce faci6?",
                "1)Bine",
                "2)Rau",
                "3)Foarte bine",
                "3",
                "Stare5");

        Assert.assertEquals(appController.createNewTest().getIntrebari().size(), 5);
    }

    @Test()
    public void testF03() throws DuplicateIntrebareException, InputValidationFailedException, NotAbleToCreateStatisticsException {

        appController.deleteIntrebari();
        appController.addIntrebare("Ce faci?",
                "1)Bine",
                "2)Rau",
                "3)Foarte bine",
                "3",
                "Stare1");
        appController.addIntrebare("Ce faci2?",
                "1)Bine",
                "2)Rau",
                "3)Foarte bine",
                "3",
                "Stare2");
        appController.addIntrebare("Ce faci3?",
                "1)Bine",
                "2)Rau",
                "3)Foarte bine",
                "3",
                "Stare3");
        appController.addIntrebare("Ce faci4?",
                "1)Bine",
                "2)Rau",
                "3)Foarte bine",
                "3",
                "Stare");

        appController.getStatistica();
    }

    @Test
    public void big_bang() throws DuplicateIntrebareException, InputValidationFailedException, NotAbleToCreateTestException, NotAbleToCreateStatisticsException {

        testF01();
        testF02();
        testF03();

    }

    @Test
    public void testPA() throws DuplicateIntrebareException, InputValidationFailedException {
        testF01();
    }

    @Test
    public void testPAB() throws DuplicateIntrebareException, InputValidationFailedException, NotAbleToCreateTestException {
        testF01();
        testF02();
    }

    @Test
    public void testPACB() throws DuplicateIntrebareException, InputValidationFailedException, NotAbleToCreateTestException, NotAbleToCreateStatisticsException {
        testF01();
        testF03();
        testF02();
    }
}
